package Java.models;

import java.util.ArrayList;


public class Book {
	private Integer id;
	private Isbn isbn;
	private Integer countnumber;
	private ArrayList<Status> status = new ArrayList<Status>();
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Isbn getIsbn() {
		return isbn;
	}
	
	public void setIsbn(Isbn isbn) {
		this.isbn = isbn;
	}
	
	public Integer getCountnumber() {
		return countnumber;
	}
	
	public void setCountnumber(Integer countnumber) {
		this.countnumber = countnumber;
	}
	
	public ArrayList<Status> getStatus() {
		return status;
	}
	
	public Status getLatestStatus() {
		return this.status.get(0);
	}
	
	public void setStatus(ArrayList<Status> status) {
		this.status = status;
	}
	
	public void addStatus(Status status) {
		this.status.add(status);
	}

}
