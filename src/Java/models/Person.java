package Java.models;

import java.util.ArrayList;
import java.util.Optional;

public class Person {
	private Integer id;
	private String firstname;
	private String lastname;
	private String email;
	private Adress adress;
	private ArrayList<Role> roles = new ArrayList<Role>();
	
	public  void setId(int i) {
		this.id = i;
	}

	public void setFirstnam(String name) {
		this.firstname = name;
	}

	public void setLastname(String name) {
		this.lastname = name;
	}

	public void setEmail(String email) {
		this.email = email;		
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public void setRoles(ArrayList<Role> roles) {
		this.roles = roles;
	}
	
	public void addRole(Role role) {
		this.roles.add(role);
	}

	public Integer getId() {
		return this.id;
	}
	
	public String getFirstname() {
		return this.firstname;
	}
	
	public String getLastname() {
		return this.lastname;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public ArrayList<Role> getRoles(){
		return this.roles;
	}
	
	public boolean hasRole(Role role){
		if(this.roles.contains(role)) {
			return true;
		}
		
		else {
			return false;
		}
	}

}
