package Java.Database.Build;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQL{

    protected Connection connection = null;
    protected String dbName = "mydatabase.db";
    protected Statement statement = null;

    public void connect(){
        if(connection != null){
            return;
        }
        try{
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
        }catch(ClassNotFoundException | SQLException e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
    
    public void close() {
    	try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private Statement Statement() {
    	if(this.statement == null) {
    		try {
				return this.statement = this.connection.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
		return statement;
    	
    }
    
    public void createAllTables() {
        for(String[] table : Constants.TABLES) {
    		if(this.tableExists(table[0])) {
    			this.createTable(table);
    			System.err.println(table[0]+ "does not exsist" + !this.tableExists(table[0]));
    		}
    		else {
    			System.err.println(table[0]+ "does exsist");
    		}
        }
    }
    
    private void createTable(String[] table) {
    	connect();
    	try {
			Statement().executeUpdate(table[1]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public boolean tableExists(String tableName){
        connect();
        try{
            DatabaseMetaData md = this.connection.getMetaData();
            ResultSet rs = md.getTables(null, null, tableName, null);
            rs.last();
            return rs.getRow() > 0;
        }catch(SQLException ex){
            
        }
        return false;
    }
}