package Java.Database.Build;

import java.util.ArrayList;

public class Constants {
	
	public static final String DATABASE_NAME = "bm";
		
	//Constants for the Firstname Table
	public static final String FIRSTNAME_TABLE = "firstname";
	public static final String FIRSTNAME_TABLE_QUERY = "create table "
			  + FIRSTNAME_TABLE
			  + "(ID integer NOT NULL, "
			  + "name varchar(40) UNIQUE, "
			  + "PRIMARY KEY (ID))";
	
	//Constants for the Lastname Table
	public static final String LASTNAME_TABLE = "lastname";
	public static final String LASTNAME_TABLE_QUERY = "create table "
			  + LASTNAME_TABLE
			  + "(ID integer NOT NULL, "
			  + "name varchar(40) UNIQUE, "
			  + "PRIMARY KEY (ID))";
	
	//Constants for the City Table
	public static final String CITY_TABLE = "city";
	public static final String CITY_TABLE_QUERY = "create table "
			+ CITY_TABLE
			+ "(ID integer NOT NULL, "
			+ "name varchar(20) NOT NULL, "
			+ "PRIMARY KEY (ID))";
	
	//Constants for the plz Table
	public static final String PLZ_TABLE = "plz";
	public static final String PLZ_TABLE_QUERY = "create table "
			+ PLZ_TABLE
			+ "(ID integer NOT NULL, "
			+ "number varchar(20) NOT NULL, "
			+ "cityID integer NOT NULL, "
			+ "PRIMARY KEY (ID), "
			+ "FOREIGN KEY (cityID) REFERENCES "
			+ CITY_TABLE + "(ID))";

	//Constants for the Street Table
	public static final String STREET_TABLE = "street";
	public static final String STREET_TABLE_QUERY = "create table "
			+ STREET_TABLE
			+ "(ID integer NOT NULL, "
			+ "name varchar(20) NOT NULL, "
			+ "PRIMARY KEY (ID))";
	
	//Constants for the Adress Table
	public static final String ADRESS_TABLE = "adress";
	public static final String ADRESS_TABLE_QUERY = "create table "
			+ ADRESS_TABLE
			+ "(ID integer NOT NULL, "
			+ "streetID integer, "
			+ "plzID integer, "
			+ "PRIMARY KEY (ID), "
			+ "FOREIGN KEY (streetID) REFERENCES "
			+ STREET_TABLE +"(ID), "
			+ "FOREIGN KEY (plzID) REFERENCES "
			+ PLZ_TABLE + "(ID))";
	
	//Constants for the Person Table
	public static final String PERSON_TABLE = "person";
	public static final String PERSON_TABLE_QUERY = "create table "
			  + PERSON_TABLE
			  + "(ID integer NOT NULL, "
			  + "firstname integer NOT NULL, "
			  + "lastname integer NOT NULL, "
			  + "email varchar(40), "
			  + "addressID integer, "
			  + "PRIMARY KEY (ID), "
			  + "FOREIGN KEY (firstname) REFERENCES "
			  + FIRSTNAME_TABLE +"(ID), "
			  + "FOREIGN KEY (lastname) REFERENCES "
			  + LASTNAME_TABLE + "(ID)"
			  + "FOREIGN KEY (addressID) REFERENCES "
			  + ADRESS_TABLE + "(ID))";
	
	//Constants for the Role Table
	public static final String ROLE_TABLE = "role";
	public static final String ROLE_TABLE_QUERY = "create table "
			  + ROLE_TABLE
			  + "(ID integer NOT NULL, "
			  + "description varchar (20), "
			  + "PRIMARY KEY (ID))";
	
	//Constants for the Person_Role Table
	public static final String PERSON_ROLE_TABLE = "person_role";
	public static final String PERSON_ROLE_TABLE_QUERY = "create table "
			  + PERSON_ROLE_TABLE
			  + "(ID integer NOT NULL, "
			  + "personID integer NOT NULL, "
			  + "roleID integer NOT NULL, "
			  + "PRIMARY KEY (ID), "
			  + "FOREIGN KEY (personID) REFERENCES "
			  + PERSON_TABLE +"(ID), "
			  + "FOREIGN KEY (roleID) REFERENCES "
			  + ROLE_TABLE + "(ID))";
	
	//Constants for the Orders Table
	public static final String ORDERS_TABLE = "orders";
	public static final String ORDERS_TABLE_QUERY = "create table "
			  + ORDERS_TABLE
			  + "(ID integer NOT NULL, "
			  + "personID integer NOT NULL, "
			  + "PRIMARY KEY (ID), "
			  + "FOREIGN KEY (personID) REFERENCES "
			  + PERSON_TABLE + "(ID))";
	
	//Constants for the subject Table
	public static final String SUBJECT_TABLE = "subject";
	public static final String SUBJECT_TABLE_QUERY = "create table "
			  + SUBJECT_TABLE
			  + "(ID integer NOT NULL, "
			  + "description varchar (20) NOT NULL, "
			  + "PRIMARY KEY (ID))";
	  
	//Constants for the statusType Table
	public static final String STATUS_TYPE_TABLE = "statusType";
	public static final String STATUS_TYPE_TABLE_QUERY = "create table "
			  + STATUS_TYPE_TABLE
			  + "(ID integer NOT NULL, "
			  + "description varchar (20) NOT NULL, "
			  + "PRIMARY KEY (ID))";
	
	//Constants for the BookTitle Table
	public static final String BOOK_TITLE_TABLE = "bookTitle";
	public static final String BOOK_TITLE_TABLE_QUERY = "create table "
			  + BOOK_TITLE_TABLE
			  + "(ID integer NOT NULL, "
			  + "name varchar (20), "
			  + "PRIMARY KEY (ID))";
	
	//Constants for the Description Table
	public static final String DESCRIPTION_TABLE = "description";
	public static final String DESCRIPTION_TABLE_QUERY = "create table "
			  + DESCRIPTION_TABLE
			  + "(ID integer NOT NULL, "
			  + "text varchar (200), "
			  + "PRIMARY KEY (ID))";
	
	//Constants for the Currency Table
	public static final String CURRENCY_TABLE = "currency";
	public static final String CURRENCY_TABLE_QUERY = "create table "
			  + CURRENCY_TABLE
			  + "(ID integer NOT NULL, "
			  + "name varchar (20) NOT NULL, "
			  + "PRIMARY KEY (ID))";
	  
	//Constants for the Prices Table
	public static final String PRICES_TABLE = "prices";
	public static final String PRICES_TABLE_QUERY = "create table "
			  + PRICES_TABLE
			  + "(ID integer NOT NULL, "
			  + "cent integer NOT NULL, "
			  + "currencyID integer NOT NULL, "
			  + "PRIMARY KEY (ID) ,"
			  + "FOREIGN KEY (currencyID) REFERENCES "
			  + CURRENCY_TABLE + "(ID))";
	
	//Constants for the ISBN Table
	public static final String ISBN_TABLE = "isbn";
	public static final String ISBN_TABLE_QUERY = "create table "
			  + ISBN_TABLE
			  + "(ID integer NOT NULL, "
			  + "ISBN TEXT NOT NULL, "
			  + "authorID integer NOT NULL, "
			  + "bookTitleID integer NOT NULL, "
			  + "descriptionID integer NOT NULL, "
			  + "pricesID integer, "
			  + "releaseDate TEXT, "
			  + "PRIMARY KEY (ID), "
			  + "FOREIGN KEY (authorID) REFERENCES "
			  + PERSON_TABLE +"(ID), "
			  + "FOREIGN KEY (bookTitleID) REFERENCES "
			  + BOOK_TITLE_TABLE +"(ID), "
			  + "FOREIGN KEY (descriptionID) REFERENCES "
			  + DESCRIPTION_TABLE + "(ID), "
			  + "FOREIGN KEY (pricesID) REFERENCES "
			  + PRICES_TABLE +"(ID))";
	
	//Constants for the ISBN_Subjects Table
	public static final String ISBN_SUBJECTS_TABLE = "isbn_subjects";
	public static final String ISBN_SUBJECTS_TABLE_QUERY = "create table "
			+ ISBN_SUBJECTS_TABLE
			+ "(ID integer NOT NULL, "
			+ "isbnID integer, "
			+ "subjectID integer, "
			+ "PRIMARY KEY (ID), "
			+ "FOREIGN KEY (isbnID) REFERENCES "
			+ ISBN_TABLE +"(ID), "
			+ "FOREIGN KEY (subjectID) REFERENCES "
			+ SUBJECT_TABLE +"(ID))";
	
	//Constants for the Book Table
	public static final String BOOK_TABLE = "book";
	public static final String BOOK_TABLE_QUERY = "create table " 
	    	  + BOOK_TABLE
	    	  + "(ID integer NOT NULL, "
	    	  + "isbnID integer NOT NULL, "
	    	  + "orderID integer NOT NULL, "
	    	  + "countNumber integer NOT NULL,"
	    	  + "PRIMARY KEY (ID), "
	    	  + "FOREIGN KEY (isbnID) REFERENCES "
	    	  + ISBN_TABLE + "(ID), "
	    	  + "FOREIGN KEY (orderID) REFERENCES "
	    	  + ORDERS_TABLE + "(ID))";
	
	//Constants for the Status Table
	public static final String STATUS_TABLE = "status";
	public static final String STATUS_TABLE_QUERY = "create table " 
	    	  + STATUS_TABLE
	    	  + "(ID integer NOT NULL, "
	    	  + "bookID integer NOT NULL, "
	    	  + "statusType integer NOT NULL, "
	    	  + "timestamp integer NOT NULL,"
	    	  + "PRIMARY KEY (ID), "
	    	  + "FOREIGN KEY (bookID) REFERENCES "
	    	  + BOOK_TABLE + "(ID), "
	    	  + "FOREIGN KEY (statusType) REFERENCES "
	    	  + STATUS_TYPE_TABLE + "(ID))";
			
	public static final String[][] TABLES;

	static { // static initializer  block.
		TABLES = new String[21][2];
		TABLES [0] 	=  new String[]{FIRSTNAME_TABLE, FIRSTNAME_TABLE_QUERY};
		TABLES [1] 	=  new String[]{LASTNAME_TABLE, LASTNAME_TABLE_QUERY};
		TABLES [3] 	=  new String[]{CITY_TABLE, CITY_TABLE_QUERY};
		TABLES [4] 	=  new String[]{PLZ_TABLE, PLZ_TABLE_QUERY};
		TABLES [5] 	=  new String[]{STREET_TABLE, STREET_TABLE_QUERY};
		TABLES [6] 	=  new String[]{ADRESS_TABLE, ADRESS_TABLE_QUERY};
		TABLES [7] 	=  new String[]{PERSON_TABLE, PERSON_TABLE_QUERY};
		TABLES [8] 	=  new String[]{ROLE_TABLE, ROLE_TABLE_QUERY};
		TABLES [9] 	=  new String[]{PERSON_ROLE_TABLE, PERSON_ROLE_TABLE_QUERY};
		TABLES [10] =  new String[]{ORDERS_TABLE, ORDERS_TABLE_QUERY};
		TABLES [11] =  new String[]{SUBJECT_TABLE, SUBJECT_TABLE_QUERY};
		TABLES [12] =  new String[]{STATUS_TYPE_TABLE, STATUS_TYPE_TABLE_QUERY};
		TABLES [13] =  new String[]{BOOK_TITLE_TABLE, BOOK_TITLE_TABLE_QUERY};
		TABLES [14] =  new String[]{DESCRIPTION_TABLE, DESCRIPTION_TABLE_QUERY};
		TABLES [15] =  new String[]{CURRENCY_TABLE, CURRENCY_TABLE_QUERY};
		TABLES [16] =  new String[]{PRICES_TABLE, PRICES_TABLE_QUERY};
		TABLES [17] =  new String[]{ISBN_TABLE, ISBN_TABLE_QUERY};
		TABLES [18] =  new String[]{ISBN_SUBJECTS_TABLE, ISBN_SUBJECTS_TABLE_QUERY};
		TABLES [19] =  new String[]{BOOK_TABLE, BOOK_TABLE_QUERY};
		TABLES [20] =  new String[]{STATUS_TABLE, STATUS_TABLE_QUERY};
	}
	
}
