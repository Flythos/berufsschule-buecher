package Java.sbm;

import Java.models.*;
import javafx.collections.*;
import javafx.collections.transformation.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;
import lombok.*;

@Getter
@Setter
public class Controller {

	@FXML
	private TableView tblContent, tblDetailed;
	@FXML
	private TableColumn<ListElement, String> colIsbn;
	@FXML
	private TableColumn<ListElement, String> colTeacherName;
	@FXML
	private TableColumn<ListElement, String> colAmount;
	@FXML
	private TableColumn<Order, String> colIsbn2;
	@FXML
	private TableColumn<Order, String> colSuffix;
	@FXML
	private TableColumn<Order, String> colStatus;
	@FXML
	private TextField txtIsbn, txtTitle, txtPublisher, txtMedium, txtAuthor, txtEdition, txtVintage, txtSuffix,
					  txtTeacherName, txtDepManager, txtDep, txtDate, filterISBN, filterTeacherName, filterAmount,
					  filterISBN2, filterSuffix, filterStatus;

	private ObservableList<Order> tblMasterData = FXCollections.observableArrayList();
	private ObservableList<ListElement> tblListElements = FXCollections.observableArrayList();

	/**
	 * TextFields Array from FXML fields after init to be used as one for all fields
	 * @return array with fields
	 */
	private TextField[] txtFieldsArr() {
		TextField[] txtFieldsArr = {this.txtIsbn, this.txtTitle, this.txtPublisher, this.txtMedium, this.txtAuthor,
									this.txtEdition, this.txtVintage, this.txtSuffix, this.txtTeacherName,
									this.txtDepManager, this.txtDep, this.txtDate, this.filterISBN,
									this.filterTeacherName, this.filterAmount, this.filterISBN2, this.filterSuffix,
									this.filterStatus};
		return txtFieldsArr;
	}


	/**
	 * Fills table with data from DB and initialises anything necessary
	 * TODO: Tabellenfüllung und initialisierung @MO
	 */
	@FXML
	public void initialize() {

		/**
		 * Verallgemeinern
		 * Auslagern in andere Methoden und dann hier aufrufen
		 * Gleiches mit Tabellenfüllung
		 */

		/**
		 * defining new dummy order with creating objects of need
		 *TODO: Remove if DB connection is working @LS
		 */
		Adress a1 = new Adress();
		a1.setCity("Redmond");
		a1.setHousenumber("1");
		a1.setPlz(98052);
		a1.setStreet("Microsoft Way");

		Person p1 = new Person();
		p1.setFirstnam("Bill");
		p1.setLastname("Gates");
		p1.setEmail("bill.gates@microsoft.com");
		p1.setAdress(a1);

		Isbn i1 = new Isbn();
		i1.setIsbn("9781440830136");


		Book b1 = new Book();
		b1.setIsbn(i1);
		b1.setCountnumber(4);


		Adress a2 = new Adress();
		a2.setCity("Palo Alto");
		a2.setHousenumber("3500");
		a2.setPlz(94304);
		a2.setStreet("Deer Creek Rd");

		Person p2 = new Person();
		p2.setFirstnam("Elon");
		p2.setLastname("Musk");
		p2.setEmail("elon.musk@tesla.com");
		p2.setAdress(a2);

		Isbn i2 = new Isbn();
		i2.setIsbn("3898799069");

		Book b2 = new Book();
		b2.setIsbn(i2);
		b2.setCountnumber(8);

		/**
		 * creating 2 dummy orders
		 */
		Order o1 = new Order();
		o1.setPerson(p1);
		o1.addBook(b1);
		Order o2 = new Order();
		o2.setPerson(p2);
		o2.addBook(b2);

		/**
		 * tblMasterData will be in a foreach and filled with data from DAO
		 * Fills tblMasterData to have an unchanged list of DB data
		 *
		 * tblMasterData.addAll(orders);
		 */
		tblMasterData.clear();
		tblMasterData.add(0, o1);
		tblMasterData.add(1, o2);

		/**
		 * loops through tblMasterData and gets respected data only for ui columns
		 */
		tblListElements.clear();
		for (Order o : tblMasterData) {
			for (Book b : o.getBooks()) {
				ListElement row = new ListElement(b.getIsbn().getIsbn(),
						(o.getPerson().getFirstname() + " " + o.getPerson().getLastname()),
						b.getCountnumber().toString());
				tblListElements.add(row);
			}
		}

		/**
		 * Initializing not done for columns
		 * colIsbn2;
		 * colSuffix;
		 * colStatus;
		 */
		colIsbn.setCellValueFactory(new PropertyValueFactory<ListElement,String>("isbn"));
		colTeacherName.setCellValueFactory(new PropertyValueFactory<ListElement,String>("teacherName"));
		colAmount.setCellValueFactory(new PropertyValueFactory<ListElement,String>("amount"));

		tblContent.setItems(tblListElements);
	}

	/**
	 * calls new order with parameter string -> String builder
	 * reset @end to clear fields
	 */
	public void save() {
		StringBuilder transmission = new StringBuilder();
		for (TextField txtField : this.txtFieldsArr()) {
			transmission.append(txtField.getText());
			transmission.append("\n");
		}
		newOrder(transmission);
		reset();
	}

	/**
	 * Resets every Field included in array & calls initialize method for refresh
	 */
	public void reset() {
		for (TextField txtField : this.txtFieldsArr()) {
			txtField.setText("");
		}
		initialize();
	}

	/**
	 * TODO: Connection to DB & transmission/insert @LS
	 * @param transmission generated string for transmission through model to DB
	 */
	public void newOrder(StringBuilder transmission) {

	}

	/**
	 * TODO: export XLS @TM
	 */
	public void exportXLS() {

	}

	/**
	 * TODO: export PDF @NW
	 */
	public void exportPDF() {
		this.printLabels();
	}

	/**
	 * TODO: import XLS @TM
	 */
	public void importXLS() {

	}

	/**
	 * TODO: Evaluation whether it must stand alone or can be combined with @exportPDF() @NW
	 */
	public void printLabels() {

	}

	/**
	 * Calls filter method with source
	 */
	public void filterTextField(KeyEvent e) {
		filter((TextField) e.getSource());
	}

	/**
	 * TODO: Filter from real data & for tblDetailed as well @MO
	 * Filters table data
	 * @param filterField source of calling filter
	 */
	private void filter(TextField filterField) {
		// 1. Wrap the ObservableList in a FilteredList (initially display all data).
		FilteredList<Order> filteredData = new FilteredList<>(tblMasterData, p -> true);

		// 2. Set the filter Predicate whenever the filter changes.
		filterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(order -> {
				// If filter text is empty, display all persons.
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}

				// Compare first name and last name of every person with filter text.
				String lowerCaseFilter = newValue.toLowerCase();

				if (order.getPerson().getFirstname().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true; // Filter matches first name.
				} else if (order.getPerson().getLastname().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true; // Filter matches last name.
				}
				return false; // Does not match.
			});
		});

		// 3. Wrap the FilteredList in a SortedList.
		SortedList<Order> sortedData = new SortedList<>(filteredData);

		// 4. Bind the SortedList comparator to the TableView comparator.
		// 	  Otherwise, sorting the TableView would have no effect.
		//sortedData.comparatorProperty().bind(tblContent.comparatorProperty());

		// 5. Add sorted (and filtered) data to the table.
		//tblContent.setItems(sortedData);
	}

}
