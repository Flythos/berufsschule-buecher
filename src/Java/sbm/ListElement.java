package Java.sbm;

import javafx.beans.property.SimpleStringProperty;
import lombok.*;

@Getter
@Setter
public class ListElement {

    private SimpleStringProperty isbn;
    private SimpleStringProperty teacherName;
    private SimpleStringProperty amount;

    public ListElement(String isbn, String teacherName, String amount) {
         this.isbn = new SimpleStringProperty(isbn);
         this.teacherName = new SimpleStringProperty(teacherName);
         this.amount = new SimpleStringProperty(amount);
    }

    public String getTeacherName() {
        return teacherName.get();
    }

    public void setTeacherName(String teacherName) {
        this.teacherName.set(teacherName);
    }

    public String getIsbn() {
        return isbn.get();
    }

    public void setIsbn(String isbn) {
        this.isbn.set(isbn);
    }

    public String getAmount() {
        return amount.get();
    }

    public void setAmount(String amount) {
        this.amount.set(amount);
    }
}
