package Java.sbm;

import java.io.File;
import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		URL url = new File("src/main/resources/sample/sbm.fxml").toURI().toURL();
		Parent root = FXMLLoader.load(url);
		primaryStage.setTitle("SBM - School Book Management");
		primaryStage.getIcons().add(new Image("file:sbm_logo.png"));
		Scene scene = new Scene(root, 1620, 980);
		scene.setFill(Color.WHITESMOKE);
		setWindowSize(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private static void setWindowSize(Stage primaryStage) {
		primaryStage.setMaximized(true);
		// primaryStage.setResizable(false);
		primaryStage.setMinWidth(1056);
		primaryStage.setMinHeight(768);
	}

}
